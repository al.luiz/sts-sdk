import CustomDOM from "./CustomDOM";
import Field from "./Field";
import Mode from "./Mode";
import Stage from "./Stage";

export class STSSDK {

    private readonly _modeSDK: Mode;
    private readonly dom: CustomDOM;
    private _formObject: HTMLElement;

    public readonly fields: Field[] = [

        new Field('client_id'),
        new Field('response_type'),
        new Field('scope'),
        new Field('redirect_uri'),
        new Field('stage'),
        new Field('nonce'),
        new Field('state')
    ];

    constructor() {
        this._modeSDK = Mode.PRODUCTION;
        this.dom = new CustomDOM();
    }

    /*
        Function Name: init
        Description: Set the STS SDK configuration
        Parameters: 
            formId: The name of the main form that will be submitted
            stage (optional): The name of the actual stage
    */
    init = async (formId: string, customInit: Function | null = null) => {

        try {

            this._formObject = this.dom.getElement(formId);

            if (this._formObject == null) { }
            //invalidArgumentError('FormId: ' + formId + ' não encontrado.');
            else {

                const formName = this.dom.getElementName(this._formObject);

                if (formName == null) { }
                //invalidArgumentError('A tag "name" do formulário deve ser preenchida.');

                if (customInit != null) {
                    await customInit();
                }

                this.configureForm();
                this.createDefaultFields();

                //logInfo('STS SDK inicializado. Formulário configurado para "' + formName + '". O Stage foi definido como "' + stage + '"');
            }

        } catch (err) {

            //logError(err);

        }

    }

    configureForm = () => {

        const actualStage: Stage = this.getActualStage() ?? Stage.LOGIN;

        switch (actualStage) {
            case Stage.LOGIN:
                this.dom.setAttr(this._formObject, 'action', './login');
                this.dom.setAttr(this._formObject, 'method', 'POST');
                break;
            case Stage.CHANGE_PASSWORD:
                this.dom.setAttr(this._formObject, 'action', './authorize');
                this.dom.setAttr(this._formObject, 'method', 'POST');
                this.createField('stage', Stage[Stage.POST_CHANGE]);
                break;
            default:
                //invalidArgumentError('O stage "' + actualStage + '" é inválido');
        }

    }

    private getActualStage = (): Stage => {

        switch (this.dom.getValueFromURL('stage')) {
            case 'login':
                return Stage.LOGIN;
            case 'change_password':
                return Stage.CHANGE_PASSWORD;
            default:
                return Stage.LOGIN;
        }


    }

    /*
    Function Name: createField
    Description: Create a new field into form element
    Parameters: 
        fieldName: The name of field that will be render
        value: The value of field
        disabled: If the field must be not submitted
*/
    public createField = (fieldName: string, value: string, disabled = false) => {

        const field: HTMLElement = this.dom.createObject('input');

        if (disabled) {
            this.dom.setAttr(field, 'disabled', 'true');
            //logDebug('Campo "' + fieldName + '" desativado. Não será enviado pelo formulário');
        }

        this.dom.setAttr(field, 'id', fieldName);
        this.dom.setAttr(field, 'name', fieldName);
        this.dom.setAttr(field, 'type', 'hidden');
        this.dom.setAttr(field, 'value', value);

        this._formObject.appendChild(field);

        //logDebug('Campo "' + fieldName + '" criado.');
    }

    /*
        Function Name: createDefaultFields
        Description: Create a default fields into form element
        Parameters: none
    */
    private createDefaultFields = () => {

        let client_id: Field;
        let redirect_uri: Field;

        for (let i: number = 0; i < this.fields.length; i++) {
            let field = this.fields[i];

            field.value = this.dom.getValueFromURL(field.name);
            
            if (field.value != null)
                this.createField(field.name, field.value);

            if (field.name == 'client_id')
                client_id = field;
            
            if (field.name == 'redirect_uri')
                redirect_uri = field;
        }

        const continueUrl: Field = this.getContinueField(client_id, redirect_uri); 
        
    }

    private getContinueField = (client_id: Field, redirect_uri: Field): Field => {

        const continueField: Field = new Field('continue');
        
        continueField.value = '/services/oauth/authorize?client_id=${client_id.value}&redirect_uri=${redirect_uri.value}&scope=openid&response_type=code';

        return continueField;

    }

    getSDKMode = () => this._modeSDK;
}
