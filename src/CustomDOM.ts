class CustomDOM {

    constructor() {
        this._urlParams = new URLSearchParams(window.location.search);
    }

    private readonly _urlParams: URLSearchParams;

    public getElement = (id: string) => document.getElementById(id);
    public createObject = (id: string) => document.createElement(id);
    public getAttr = (obj: HTMLElement, attributeName: string) => obj.getAttribute(attributeName);
    public setAttr = (obj: HTMLElement, attributeName: string, value: string) => obj.setAttribute(attributeName, value);
    public getElementName = (obj: HTMLElement) => this.getAttr(obj, 'name');
    public isNullOrUnd = (obj: object) => obj == null || obj == undefined;
    
    public getValueFromURL = (fieldName: string) => this._urlParams.get(fieldName);
}

export default CustomDOM;
