class Field {

    constructor(name: string) {
        this.name = name;
    }

    name: string;
    value: string | null;
    description: string | null;
}

export default Field;
